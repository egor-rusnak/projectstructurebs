using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProjectStructureBs.BLL.Interfaces;
using ProjectStructureBs.BLL.MappingProfiles;
using ProjectStructureBs.BLL.Services;
using ProjectStructureBs.DAL.Data;
using ProjectStructureBs.DAL.Entities;
using System;

namespace ProjectStructureBs.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<UserProfile>();
            });

            services.AddControllers();

            services.AddSingleton(typeof(Storage<>));
            services.AddScoped(typeof(IRepository<>), typeof(StorageRepository<>));
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<IQueryService, QueryService>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITaskService, TaskService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            SeedData(app.ApplicationServices);
        }

        private void SeedData(IServiceProvider provider)
        {
            var teamsStorage = provider.GetService<Storage<Team>>();
            teamsStorage.Add(new Team(1, "Bananas", DateTime.Now.AddDays(new Random().Next() % 30 - 15)));
            teamsStorage.Add(new Team(2, "Changes", DateTime.Now.AddDays(new Random().Next() % 30 - 15)));
            teamsStorage.Add(new Team(3, "Packages", DateTime.Now.AddDays(new Random().Next() % 30 - 15)));
            teamsStorage.Add(new Team(4, "asdas", DateTime.Now.AddDays(new Random().Next() % 30 - 15)));

            var usersStorage = provider.GetService<Storage<User>>();
            usersStorage.Add(new User(1, "John", "Jonson", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500), 1));
            usersStorage.Add(new User(2, "Marchel", "Davidson", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500), 3));
            usersStorage.Add(new User(3, "Anchey", "Brownly", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500), 3));
            usersStorage.Add(new User(4, "Boris", "Apostolov", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500), 2));
            usersStorage.Add(new User(5, "Grace", "Donalds", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500), 2));
            usersStorage.Add(new User(6, "George", "Jakson", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500), null));
            usersStorage.Add(new User(7, "Annet", "Wonderson", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500), null));
            usersStorage.Add(new User(8, "Marcus", "Jonson", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500), 1));
            usersStorage.Add(new User(9, "Marcus", "Jonson", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddYears(-100), 4));


            var projectsStorage = provider.GetService<Storage<Project>>();
            projectsStorage.Add(new Project(1, "ProjName1", "Des1", 1, 1, DateTime.Now.AddDays(new Random().Next() % 3000 - 2500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500)));
            projectsStorage.Add(new Project(2, "ProjName2", "Des2", 5, 1, DateTime.Now.AddDays(new Random().Next() % 3000 - 2500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500)));
            projectsStorage.Add(new Project(3, "ProjName3", "Des3", 8, 2, DateTime.Now.AddDays(new Random().Next() % 3000 - 2500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500)));
            projectsStorage.Add(new Project(4, "ProjName4", "Des4", 7, 3, DateTime.Now.AddDays(new Random().Next() % 3000 - 2500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500)));

            var tasksStorage = provider.GetService<Storage<Task>>();
            tasksStorage.Add(new Task(0, "task1", 1, 1, "no", DateTime.Now.AddDays(new Random().Next() % 30 - 15), null));
            tasksStorage.Add(new Task(0, "task2", 1, 1, "no", DateTime.Now.AddDays(new Random().Next() % 30 - 15), DateTime.Now.AddDays(new Random().Next() % 30 - 15)));
            tasksStorage.Add(new Task(0, "task3", 1, 1, "no", DateTime.Now.AddDays(new Random().Next() % 30 - 15), null));

        }
    }
}
