﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructureBs.BLL.Interfaces;
using ProjectStructureBs.Common.DTOs;
using System.Collections.Generic;

namespace ProjectStructureBs.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TeamDto>> GetAll()
        {
            return Ok(_teamService.GetAllTeams());
        }
        [HttpGet("{id}")]
        public ActionResult<TeamDto> Get(int id)
        {
            var team = _teamService.GetById(id);
            if (team == null) return NotFound();

            return Ok(team);
        }

        [HttpPost]
        public ActionResult<TeamDto> CreateTeam([FromBody] TeamDto team)
        {
            var result = _teamService.AddTeam(team);
            return CreatedAtAction(nameof(Get), new { id = result.Id }, result);
        }

        [HttpPut]
        public ActionResult<TeamDto> UpdateTeam([FromBody] TeamDto team)
        {
            var result = _teamService.UpdateTeam(team);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProject(int id)
        {
            _teamService.RemoveTeam(id);
            return NoContent();
        }
    }
}
