﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructureBs.BLL.Interfaces;
using ProjectStructureBs.Common.DTOs.Project;
using System.Collections.Generic;

namespace ProjectStructureBs.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDto>> GetAll()
        {
            return Ok(_projectService.GetAllProjects());
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDto> Get(int id)
        {
            var project = _projectService.GetById(id);
            if (project == null) return NotFound();

            return Ok(project);
        }

        [HttpPost]
        public ActionResult<ProjectDto> CreateProject([FromBody] ProjectDto project)
        {
            var result = _projectService.AddProject(project);
            return CreatedAtAction(nameof(Get), new { id = result.Id }, result);
        }

        [HttpPut]
        public ActionResult<ProjectDto> UpdateProject([FromBody] ProjectDto project)
        {
            var result = _projectService.UpdateProject(project);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProject(int id)
        {
            _projectService.RemoveProject(id);
            return NoContent();
        }

    }
}
