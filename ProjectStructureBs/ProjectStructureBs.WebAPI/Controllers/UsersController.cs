﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructureBs.BLL.Interfaces;
using ProjectStructureBs.Common.DTOs.Project;
using ProjectStructureBs.Common.DTOs.User;
using System.Collections.Generic;

namespace ProjectStructureBs.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDto>> GetAll()
        {
            return Ok(_userService.GetAllUsers());
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDto> Get(int id)
        {
            var user = _userService.GetById(id);
            if (user == null) return NotFound();

            return Ok(user);
        }

        [HttpPost]
        public ActionResult<ProjectDto> CreateUser([FromBody] UserDto user)
        {
            var result = _userService.AddUser(user);
            return CreatedAtAction(nameof(Get), new { id = result.Id }, result);
        }

        [HttpPut]
        public ActionResult<ProjectDto> UpdateUser([FromBody] UserDto user)
        {
            var result = _userService.UpdateUser(user);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProject(int id)
        {
            _userService.RemoveUser(id);
            return NoContent();
        }

    }
}
