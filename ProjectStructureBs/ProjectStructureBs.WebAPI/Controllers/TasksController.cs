﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructureBs.BLL.Interfaces;
using ProjectStructureBs.Common.DTOs;
using System.Collections.Generic;

namespace ProjectStructureBs.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TaskDto>> GetAll()
        {
            return Ok(_taskService.GetAllTasks());
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDto> Get(int id)
        {
            var task = _taskService.GetById(id);
            if (task == null) return NotFound();

            return Ok(task);
        }

        [HttpPost]
        public ActionResult<TaskDto> CreateTask([FromBody] TaskDto task)
        {
            var result = _taskService.AddTask(task);
            return CreatedAtAction(nameof(Get), new { id = result.Id }, result);
        }

        [HttpPut]
        public ActionResult<TaskDto> UpdateTask([FromBody] TaskDto task)
        {
            var result = _taskService.UpdateTask(task);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTask(int id)
        {
            _taskService.RemoveTask(id);
            return NoContent();
        }
    }
}
