﻿using AutoMapper;
using ProjectStructureBs.BLL.Interfaces;
using ProjectStructureBs.Common.DTOs;
using ProjectStructureBs.Common.DTOs.Project;
using ProjectStructureBs.Common.DTOs.User;
using ProjectStructureBs.DAL.Data;
using ProjectStructureBs.DAL.Entities;
using ProjectStructureBs.DAL.Entities.Comparers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructureBs.BLL.Services
{
    public class QueryService : IQueryService
    {
        private readonly IRepository<Project> _projectsRepository;
        private readonly IRepository<Team> _teamsRepository;
        private readonly IRepository<Task> _tasksRepository;
        private readonly IRepository<User> _usersRepository;
        private readonly IMapper _mapper;

        private readonly IEnumerable<Project> _projects;

        public QueryService(IRepository<Project> projects, IRepository<Team> teams, IRepository<Task> tasks, IRepository<User> users, IMapper mapper)
        {
            _projectsRepository = projects;
            _teamsRepository = teams;
            _tasksRepository = tasks;
            _usersRepository = users;
            _mapper = mapper;

            _projects = GetProjects();
        }

        public Dictionary<ProjectDto, int> ExecuteQueryOne(int userId)
        {
            return new Dictionary<ProjectDto, int>(_projects
                .SelectMany(p => p.Tasks)
                .Where(t => t.Performer.Id == userId)
                .GroupBy(t => t.ProjectId)
                .Join(_projects, groupTasks => groupTasks.Key, p => p.Id, (groupTasks, p) =>
                      new KeyValuePair<ProjectDto, int>(_mapper.Map<ProjectDto>(p), groupTasks.Count()))) ?? new Dictionary<ProjectDto, int>();
        }

        public IEnumerable<TaskDto> ExecuteQueryTwo(int userId)
        {
            var result = _projects.SelectMany(p => p.Tasks?.Where(t => t.Performer.Id == userId && t.Name.Length < 45));
            return _mapper.Map<IEnumerable<TaskDto>>(result.ToList() ?? new List<Task>());
        }

        // In key - Id, value - name
        public IEnumerable<KeyValuePair<int, string>> ExecuteQueryThree(int userId)
        {
            return _projects
                .SelectMany(p => p.Tasks
                    .Where(t =>
                        t.Performer.Id == userId
                        && t.FinishedAt.HasValue
                        && t.FinishedAt.Value.Year == DateTime.Now.Year
                    ).Select(t => new KeyValuePair<int, string>(t.Id, t.Name))
                ).ToList();
        }

        //In key - Id, value - name, User - UsersList
        public IEnumerable<KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>> ExecuteQueryFour()
        {
            return _projects.Select(p => p.Team).Distinct(new TeamsEqualityComparer())
                .Join(
                    _projects.Select(p => p.Author)
                        .Union(_projects.SelectMany(p => p.Tasks.Select(t => t.Performer)))
                        .Distinct(new UsersEqualityComparer())
                        .OrderByDescending(u => u.RegisteredAt),
                    t => t.Id, u => u.TeamId, (t, u) => new { Team = t, User = u }
                )
                .GroupBy(t => new KeyValuePair<int, string>(t.Team.Id, t.Team.Name), e => _mapper.Map<UserDto>(e.User))
                .Where(t => !t.Any(u => u.BirthDay.Year >= DateTime.Now.Year - 10))
                .Select(t=>new KeyValuePair<KeyValuePair<int,string>,IEnumerable<UserDto>>(new KeyValuePair<int, string>(t.Key.Key,t.Key.Value),t.Select(m=>m).ToList()))
                .ToList();
        }

        public IEnumerable<KeyValuePair<UserDto, IEnumerable<TaskDto>>> ExecuteQueryFive()
        {
            return _projects.SelectMany(p => p.Tasks.Select(t => t.Performer)
                .Union(_projects.Select(m => m.Author)))
                .Distinct(new UsersEqualityComparer())
                .GroupJoin(
                    _projects.SelectMany(p => p.Tasks),
                        u => u.Id,
                        t => t.Performer.Id,
                        (u, t) => new KeyValuePair<UserDto, IEnumerable<TaskDto>>(_mapper.Map<UserDto>(u), _mapper.Map<IEnumerable<TaskDto>>(t.OrderByDescending(tsk => tsk.Name.Length))))
                .OrderBy(u => u.Key.FirstName)
                .ToList();
        }

        public UserInfoDto ExecuteQuerySix(int userId)
        {
            return _projects.SelectMany(p => p.Tasks.Select(t => t.Performer))
                .Union(_projects.Select(p => p.Author))
                .Distinct(new UsersEqualityComparer())
                .GroupJoin
                (
                    _projects,
                    u => u.TeamId,
                    p => p.Team.Id,
                    (u, p) =>
                      new { User = u, LastProject = p.OrderByDescending(e => e.CreatedAt).First() }
                )
                .GroupJoin(
                    _projects.SelectMany(p => p.Tasks),
                    u => u.User.Id,
                    t => t.Performer.Id,
                    (u, t) => new { u.User, u.LastProject, Tasks = t }
                )
                .Select(e => new UserInfoDto(
                    _mapper.Map<UserDto>(e.User),
                    _mapper.Map<ProjectDto>(e.LastProject),
                    e.LastProject.Tasks.Count(),
                    e.Tasks?.Count(t => !t.FinishedAt.HasValue) ?? 0,
                    _mapper.Map<TaskDto>(e.Tasks?.OrderByDescending(t => (t.FinishedAt ?? DateTime.Now) - t.CreatedAt).FirstOrDefault())
                )).Where(e => e.LastProject != null)
                .FirstOrDefault(r => r.User.Id == userId) ?? throw new ArgumentException("User doesn't have projects!");
        }

        public IEnumerable<ProjectInfoDto> ExecuteQuerySeven()
        {
            return _projects.Select(p => new ProjectInfoDto(
                    _mapper.Map<ProjectDto>(p),
                    _mapper.Map<TaskDto>(p.Tasks.OrderByDescending(t => t.Description.Length)?.FirstOrDefault()),
                    _mapper.Map<TaskDto>(p.Tasks.OrderBy(t => t.Name.Length)?.FirstOrDefault()),
                    p.Description.Length > 20 || p.Tasks.Count() < 3 ?
                        _projects.Select(pr => pr.Author)
                        .Union(_projects.SelectMany(pr => pr.Tasks.Select(t => t.Performer)))
                        .Distinct(new UsersEqualityComparer())
                        .Where(u => u.TeamId == p.Team.Id)?.Count() ?? null : null)
                ).ToList();
        }

        private IEnumerable<Project> GetProjects()
        {
            var projects = _projectsRepository.GetAll();
            var tasks = _tasksRepository.GetAll();
            var teams = _teamsRepository.GetAll();
            var users = _usersRepository.GetAll();

            var usersEntities = users.Select(u => new User(u.Id, u.FirstName, u.LastName, u.Email, u.RegisteredAt, u.BirthDay, u.TeamId));

            var projectEntities = projects
                .GroupJoin(tasks, p => p.Id, t => t.ProjectId, (p, t) => new { Project = p, Tasks = t })
                .Join(teams, p => p.Project.TeamId, t => t.Id, (p, t) => new { p.Project, Team = new Team(t.Id, t.Name, t.CreatedAt), p.Tasks })
                .Select(p =>
                {
                    var author = usersEntities.FirstOrDefault(u => u.Id == p.Project.AuthorId);
                    var team = p.Team;
                    return new Project(p.Project.Id,
                            p.Project.Name,
                            p.Project.Description,
                            author.Id,
                            team.Id,
                            p.Project.Deadline,
                            p.Project.CreatedAt
                            )
                    {
                        Author = author,
                        Team = team,
                        Tasks = p.Tasks.Select(t =>
                        new Task(t.Id, t.Name, usersEntities.FirstOrDefault(u => u.Id == t.PerformerId).Id, t.ProjectId, t.Description, t.CreatedAt, t.FinishedAt)
                        {
                            Performer = usersEntities.FirstOrDefault(u => u.Id == t.PerformerId)
                        })
                    };
                });
            return projectEntities;
        }
    }
}
