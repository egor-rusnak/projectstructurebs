﻿using AutoMapper;
using ProjectStructureBs.BLL.Interfaces;
using ProjectStructureBs.Common.DTOs.Project;
using ProjectStructureBs.DAL.Data;
using ProjectStructureBs.DAL.Entities;
using System.Collections.Generic;

namespace ProjectStructureBs.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IRepository<Project> _projectRepository;
        private readonly IMapper _mapper;

        public ProjectService(IRepository<Project> projectRepository, IMapper mapper)
        {
            _projectRepository = projectRepository;
            _mapper = mapper;
        }

        public ProjectDto AddProject(ProjectDto newProject)
        {
            var projectEntity = _mapper.Map<Project>(newProject);
            return _mapper.Map<ProjectDto>(_projectRepository.Create(projectEntity));
        }

        public void RemoveProject(int id)
        {
            _projectRepository.Delete(id);
        }

        public ProjectDto UpdateProject(ProjectDto project)
        {
            var projectEntity = _mapper.Map<Project>(project);
            return _mapper.Map<ProjectDto>(_projectRepository.Update(projectEntity));
        }

        public IEnumerable<ProjectDto> GetAllProjects()
        {
            var elems = _projectRepository.GetAll();
            return _mapper.Map<IEnumerable<ProjectDto>>(elems);
        }

        public ProjectDto GetById(int id)
        {
            return _mapper.Map<ProjectDto>(_projectRepository.GetById(id));
        }
    }
}
