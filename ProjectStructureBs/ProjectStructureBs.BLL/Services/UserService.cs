﻿using AutoMapper;
using ProjectStructureBs.BLL.Interfaces;
using ProjectStructureBs.Common.DTOs.User;
using ProjectStructureBs.DAL.Data;
using ProjectStructureBs.DAL.Entities;
using System.Collections.Generic;

namespace ProjectStructureBs.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _users;
        private readonly IMapper _mapper;

        public UserService(IRepository<User> users, IMapper mapper)
        {
            _users = users;
            _mapper = mapper;
        }

        public UserDto AddUser(UserDto user)
        {
            var userEntity = _mapper.Map<User>(user);
            return _mapper.Map<UserDto>(_users.Create(userEntity));
        }

        public UserDto UpdateUser(UserDto user)
        {
            var userEntity = _mapper.Map<User>(user);
            return _mapper.Map<UserDto>(_users.Update(userEntity));
        }

        public void RemoveUser(int id)
        {
            _users.Delete(id);
        }

        public IEnumerable<UserDto> GetAllUsers()
        {
            return _mapper.Map<IEnumerable<UserDto>>(_users.GetAll());
        }

        public UserDto GetById(int id)
        {
            return _mapper.Map<UserDto>(_users.GetById(id));
        }
    }
}
