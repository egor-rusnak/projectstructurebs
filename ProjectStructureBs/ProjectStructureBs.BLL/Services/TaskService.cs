﻿using AutoMapper;
using ProjectStructureBs.BLL.Interfaces;
using ProjectStructureBs.Common.DTOs;
using ProjectStructureBs.DAL.Data;
using ProjectStructureBs.DAL.Entities;
using System.Collections.Generic;

namespace ProjectStructureBs.BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IRepository<Task> _tasks;
        private readonly IMapper _mapper;

        public TaskService(IRepository<Task> tasks, IMapper mapper)
        {
            _tasks = tasks;
            _mapper = mapper;
        }

        public TaskDto AddTask(TaskDto task)
        {
            var taskEntity = _mapper.Map<Task>(task);
            return _mapper.Map<TaskDto>(_tasks.Create(taskEntity));
        }

        public TaskDto UpdateTask(TaskDto task)
        {
            var taskEntity = _mapper.Map<Task>(task);
            return _mapper.Map<TaskDto>(_tasks.Update(taskEntity));
        }

        public void RemoveTask(int id)
        {
            _tasks.Delete(id);
        }

        public IEnumerable<TaskDto> GetAllTasks()
        {
            return _mapper.Map<IEnumerable<TaskDto>>(_tasks.GetAll());
        }

        public TaskDto GetById(int id)
        {
            return _mapper.Map<TaskDto>(_tasks.GetById(id));
        }
    }
}
