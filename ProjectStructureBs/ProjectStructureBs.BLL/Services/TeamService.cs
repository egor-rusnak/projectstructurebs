﻿using AutoMapper;
using ProjectStructureBs.BLL.Interfaces;
using ProjectStructureBs.Common.DTOs;
using ProjectStructureBs.DAL.Data;
using ProjectStructureBs.DAL.Entities;
using System.Collections.Generic;

namespace ProjectStructureBs.BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IRepository<Team> _teams;
        private readonly IMapper _mapper;

        public TeamService(IRepository<Team> teams, IMapper mapper)
        {
            _teams = teams;
            _mapper = mapper;
        }

        public TeamDto AddTeam(TeamDto team)
        {
            var teamEntity = _mapper.Map<Team>(team);
            return _mapper.Map<TeamDto>(_teams.Create(teamEntity));
        }

        public TeamDto UpdateTeam(TeamDto team)
        {
            var teamEntity = _mapper.Map<Team>(team);
            return _mapper.Map<TeamDto>(_teams.Update(teamEntity));
        }

        public void RemoveTeam(int id)
        {
            _teams.Delete(id);
        }

        public IEnumerable<TeamDto> GetAllTeams()
        {
            return _mapper.Map<IEnumerable<TeamDto>>(_teams.GetAll());
        }

        public TeamDto GetById(int id)
        {
            return _mapper.Map<TeamDto>(_teams.GetById(id));
        }
    }
}
