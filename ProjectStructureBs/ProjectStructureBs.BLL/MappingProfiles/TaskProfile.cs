﻿using AutoMapper;
using ProjectStructureBs.Common.DTOs;
using ProjectStructureBs.DAL.Entities;


namespace ProjectStructureBs.BLL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDto>();
            CreateMap<TaskDto, Task>();
        }
    }
}
