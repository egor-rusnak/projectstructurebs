﻿using AutoMapper;
using ProjectStructureBs.Common.DTOs;
using ProjectStructureBs.DAL.Entities;

namespace ProjectStructureBs.BLL.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDto>();
            CreateMap<TeamDto, Team>();
        }
    }
}
