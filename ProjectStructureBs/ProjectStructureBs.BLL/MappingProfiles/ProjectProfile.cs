﻿using AutoMapper;
using ProjectStructureBs.Common.DTOs.Project;
using ProjectStructureBs.DAL.Entities;

namespace ProjectStructureBs.BLL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDto>();
            CreateMap<ProjectDto, Project>();
        }
    }
}
