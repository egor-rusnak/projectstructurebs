﻿using AutoMapper;
using ProjectStructureBs.Common.DTOs.User;
using ProjectStructureBs.DAL.Entities;

namespace ProjectStructureBs.BLL.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
        }
    }
}
