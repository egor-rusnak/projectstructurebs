﻿using ProjectStructureBs.Common.DTOs.User;
using System.Collections.Generic;

namespace ProjectStructureBs.BLL.Interfaces
{
    public interface IUserService
    {
        UserDto AddUser(UserDto user);
        IEnumerable<UserDto> GetAllUsers();
        UserDto GetById(int id);
        void RemoveUser(int id);
        UserDto UpdateUser(UserDto user);
    }
}