﻿using ProjectStructureBs.Common.DTOs.Project;
using System.Collections.Generic;

namespace ProjectStructureBs.BLL.Interfaces
{
    public interface IProjectService
    {
        ProjectDto AddProject(ProjectDto newProject);
        IEnumerable<ProjectDto> GetAllProjects();
        ProjectDto GetById(int id);
        void RemoveProject(int id);
        ProjectDto UpdateProject(ProjectDto project);
    }
}