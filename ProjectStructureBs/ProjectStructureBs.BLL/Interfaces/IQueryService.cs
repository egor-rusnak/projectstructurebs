﻿using ProjectStructureBs.Common.DTOs;
using ProjectStructureBs.Common.DTOs.Project;
using ProjectStructureBs.Common.DTOs.User;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructureBs.BLL.Interfaces
{
    public interface IQueryService
    {
        IEnumerable<KeyValuePair<UserDto, IEnumerable<TaskDto>>> ExecuteQueryFive();
        IEnumerable<KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>> ExecuteQueryFour();
        Dictionary<ProjectDto, int> ExecuteQueryOne(int userId);
        IEnumerable<ProjectInfoDto> ExecuteQuerySeven();
        UserInfoDto ExecuteQuerySix(int userId);
        IEnumerable<KeyValuePair<int, string>> ExecuteQueryThree(int userId);
        IEnumerable<TaskDto> ExecuteQueryTwo(int userId);
    }
}