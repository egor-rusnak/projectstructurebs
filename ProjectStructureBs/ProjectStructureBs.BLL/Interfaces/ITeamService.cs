﻿using ProjectStructureBs.Common.DTOs;
using System.Collections.Generic;

namespace ProjectStructureBs.BLL.Interfaces
{
    public interface ITeamService
    {
        TeamDto AddTeam(TeamDto team);
        IEnumerable<TeamDto> GetAllTeams();
        TeamDto GetById(int id);
        void RemoveTeam(int id);
        TeamDto UpdateTeam(TeamDto team);
    }
}