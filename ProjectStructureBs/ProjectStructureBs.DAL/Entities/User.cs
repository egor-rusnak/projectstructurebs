﻿using ProjectStructureBs.DAL.Entities.Abstraction;
using System;

namespace ProjectStructureBs.DAL.Entities
{
    public class User : BaseEntity<User>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
        public int? TeamId { get; set; }

        public User(int id, string firstName, string lastName, string email, DateTime registeredAt, DateTime birthDay, int? teamId) : base(id)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            RegisteredAt = registeredAt;
            BirthDay = birthDay;
            TeamId = teamId;
        }

        public override string ToString()
        {
            return Id + "-" + FirstName + " " + LastName;
        }

        public override void Update(User another)
        {
            FirstName = another.FirstName;
            LastName = another.LastName;
            Email = another.Email;
            RegisteredAt = another.RegisteredAt;
            BirthDay = another.BirthDay;
            TeamId = another.TeamId;
        }
    }
}
