﻿using ProjectStructureBs.DAL.Entities.Abstraction;
using System;
using System.Collections.Generic;

namespace ProjectStructureBs.DAL.Entities
{
    public class Project : BaseEntity<Project>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int AuthorId { get; set; }
        public User Author { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
        public IEnumerable<Task> Tasks { get; set; }

        public Project(int id, string name, string description, int authorId, int teamId, DateTime deadline, DateTime createdAt) : base(id)
        {
            Description = description;
            Name = name;
            AuthorId = authorId;
            TeamId = teamId;
            Deadline = deadline;
            CreatedAt = createdAt;
        }

        public override void Update(Project another)
        {
            Description = another.Description;
            Name = another.Name;
            AuthorId = another.AuthorId;
            TeamId = another.TeamId;
            Deadline = another.Deadline;
            CreatedAt = another.CreatedAt;
        }
    }
}
