﻿using ProjectStructureBs.DAL.Entities.Abstraction;
using System;

namespace ProjectStructureBs.DAL.Entities
{
    public class Team : BaseEntity<Team>
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public Team(int id, string name, DateTime createdAt) : base(id)
        {
            Name = name;
            CreatedAt = createdAt;
        }

        public override void Update(Team another)
        {
            Name = another.Name;
            CreatedAt = another.CreatedAt;
        }
    }
}
