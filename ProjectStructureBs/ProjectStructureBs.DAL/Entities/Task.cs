﻿using ProjectStructureBs.DAL.Entities.Abstraction;
using System;

namespace ProjectStructureBs.DAL.Entities
{
    public class Task : BaseEntity<Task>
    {
        public string Name { get; set; }
        public int PerformerId { get; set; }
        public User Performer { get; set; }
        public int ProjectId { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public Task(int id, string name, int performerId, int projectId, string description, DateTime createdAt, DateTime? finishedAt) : base(id)
        {
            PerformerId = performerId;
            ProjectId = projectId;
            Description = description;
            CreatedAt = createdAt;
            FinishedAt = finishedAt;
            Name = name;
        }

        public override void Update(Task another)
        {
            PerformerId = another.PerformerId;
            ProjectId = another.ProjectId;
            Description = another.Description;
            CreatedAt = another.CreatedAt;
            FinishedAt = another.FinishedAt;
            Name = another.Name;
        }
    }
}
