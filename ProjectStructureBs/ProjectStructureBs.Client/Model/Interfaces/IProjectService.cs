﻿using ProjectStructureBs.Common.DTOs;
using ProjectStructureBs.Common.DTOs.Project;
using ProjectStructureBs.Common.DTOs.User;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructureBs.UI.Model.Interfaces
{
    public interface IProjectService : IDisposable
    {
        IEnumerable<KeyValuePair<UserDto, List<TaskDto>>> ExecuteQueryFive();
        IEnumerable<KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>> ExecuteQueryFour();
        Dictionary<ProjectDto, int> ExecuteQueryOne(int userId);
        IEnumerable<ProjectInfoDto> ExecuteQuerySeven();
        UserInfoDto ExecuteQuerySix(int userId);
        IEnumerable<KeyValuePair<int, string>> ExecuteQueryThree(int userId);
        IEnumerable<TaskDto> ExecuteQueryTwo(int userId);
    }
}