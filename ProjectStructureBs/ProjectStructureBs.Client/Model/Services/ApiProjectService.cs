﻿using ProjectStructureBs.Common.DTOs;
using ProjectStructureBs.Common.DTOs.Project;
using ProjectStructureBs.Common.DTOs.User;
using ProjectStructureBs.UI.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;

namespace ProjectStructureBs.UI.Model.Services
{
    public class ApiProjectService : IProjectService
    {
        private readonly HttpClient _client;
        public ApiProjectService(Uri apiAdress)
        {
            _client = new HttpClient()
            {
                BaseAddress = apiAdress
            };
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        public IEnumerable<KeyValuePair<UserDto, List<TaskDto>>> ExecuteQueryFive()
        {
            return _client.GetFromJsonAsync<List<KeyValuePair<UserDto, List<TaskDto>>>>("api/Queries/Fifth").Result;
        }

        public IEnumerable<KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>> ExecuteQueryFour()
        {
            return _client.GetFromJsonAsync<List<KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>>>("api/Queries/Fourth").Result;
        }

        public Dictionary<ProjectDto, int> ExecuteQueryOne(int userId)
        {
            return new Dictionary<ProjectDto, int>(_client.GetFromJsonAsync<List<KeyValuePair<ProjectDto, int>>>($"api/Queries/First/{userId}").Result);
        }

        public IEnumerable<ProjectInfoDto> ExecuteQuerySeven()
        {
            return _client.GetFromJsonAsync<List<ProjectInfoDto>>("api/Queries/Seventh").Result;
        }

        public UserInfoDto ExecuteQuerySix(int userId)
        {
            return _client.GetFromJsonAsync<UserInfoDto>($"api/Queries/Sixth/{userId}").Result;
        }

        public IEnumerable<KeyValuePair<int, string>> ExecuteQueryThree(int userId)
        {
            return _client.GetFromJsonAsync<List<KeyValuePair<int, string>>>($"api/Queries/Third/{userId}").Result;
        }

        public IEnumerable<TaskDto> ExecuteQueryTwo(int userId)
        {
            return _client.GetFromJsonAsync<List<TaskDto>>($"api/Queries/Second/{userId}").Result;
        }
    }
}
