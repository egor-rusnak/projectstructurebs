﻿using ProjectStructureBs.UI.Model.Interfaces;
using System;

namespace ProjectStructureBs.Application.Commands
{
    public class ExecuteSixthQuery : ICommand
    {
        public void Execute(IProjectService projectService)
        {
            Console.Write("Input a user Id: ");
            if (int.TryParse(Console.ReadLine(), out int input))
            {
                Console.WriteLine($"Result for user {input}: ");
                var result = projectService.ExecuteQuerySix(input);
                if (result != null)
                {
                    Console.WriteLine($"[{result.User}]");
                    Console.WriteLine($"Last Project by Created date: {result.LastProject.Id} {result.LastProject.Name}");
                    Console.WriteLine($"Count of tasks in last project: {result.LastProjectTasksCount}");
                    Console.WriteLine($"Unfinished tasks: {result.UnfinishedTasksCount}");
                    Console.WriteLine("Longest task (if finished is null than using now): " + (result.LongestTask?.ToString() ?? "No tasks!"));
                }
                else Console.WriteLine("BLANK(No user in Data)");
            }
        }
    }
}
