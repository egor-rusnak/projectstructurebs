﻿using ProjectStructureBs.UI.Model.Interfaces;

namespace ProjectStructureBs.Application.Commands
{
    public interface ICommand
    {
        public void Execute(IProjectService projectService);
    }
}
