﻿using ProjectStructureBs.Common.DTOs.Abstraction;
using System;

namespace ProjectStructureBs.Common.DTOs
{
    public class TeamDto : BaseEntityDto
    {
        public DateTime CreatedAt { get; set; }
        public string Name { get; set; }
    }
}
