﻿using ProjectStructureBs.Common.DTOs.Abstraction;
using System;

namespace ProjectStructureBs.Common.DTOs.Project
{
    public class ProjectDto : BaseEntityDto
    {
        public string Name { get; set; }
        public int AuthorId { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
        public int TeamId { get; set; }
        public override string ToString()
        {
            return Id + "-" + Name;
        }
    }
}
